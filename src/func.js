const getSum = (str1, str2) => 
{
    
      if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;
      if (str1.length === 0) str1 = '0';
      if (str2.length === 0) str2 = '0';
      const DELTA = /^\d+$/;
      if (!DELTA.test(str1) || !DELTA.test(str2)) return false;
      const summa = parseInt(str1) + parseInt(str2);
      return String(summa);
    
    

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count_author = 0;
  let count_comment = 0;
  for (const post of listOfPosts)
  {
    if (post.author===authorName)
      {
      count_author++;
      }
     if (undefined !== post.comments && post.hasOwnProperty('comments'))
      {
        for (const element of post.comments)
        {
          if (element.author===authorName)
          {
          count_comment++;
          }
        }
      }
  }
  return "Post:"+String(count_author)+",comments:"+String(count_comment);
};

const tickets=(people)=> {
  
  let cashRegisterObject = {25:0, 50:0, 100:0};
  const cashUpdait = paid =>
  {
    let banknote = [25,50,100];
    for (let i=0; i < 2; i++)
    {
       cashRegisterObject[banknote[i]]= cashRegisterObject[banknote[i]] + paid[i];
    }
  }
  for (let i in people) 
  {
    if (people[i] == 25)
    {
       cashUpdait([1, 0, 0]);
    }
    else
    if (people[i] ==  50) 
    {
      cashUpdait([-1, 1, 0]);
    }
    else 
     {
      if (cashRegisterObject[25] >= 1 && cashRegisterObject[50] >= 1)
      {
        cashUpdait([-1, -1, 1]);
      } 
      else
      {
        cashUpdait([-3, 0, 1]);
      } 
    } 
  }
  const verdict = (cashRegisterObject[25] < 0 || cashRegisterObject[50] < 0 || cashRegisterObject[100] < 0);
  return verdict ? 'NO' : 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
